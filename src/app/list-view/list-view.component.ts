import { Component, OnInit, OnDestroy } from '@angular/core';
import { Task } from '../data/task.model';
import { StorageService } from '../services/storage.service';
import { Subscription } from 'rxjs';
import { TaskStatus } from '../data/task-status.enum';
import { ConfirmModalService } from '../confirm-modal/confirm-modal.service';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent implements OnInit, OnDestroy {

  public tasks: Task[];

  public TaskStatus= TaskStatus;

  private subscription: Subscription | null;

  private deleteSubscr: Subscription;

  constructor(public storageService: StorageService,
    private modalService: ConfirmModalService) { }

  ngOnInit(): void {
    /*
    If using onDataChaged observable
    this.tasks = this.storageService.all();
    this.subscription = this.storageService.onDataChanged$
      .subscribe(() => this.tasks = this.storageService.all());
    */

    // we even dont need this if we use the async pype which des the sucsribe/unsubscribe
    //this.subscription = this.storageService.all$.subscribe((v) => this.tasks = v); 
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }

  changeStatus(id:number, newStatus: TaskStatus) {
    this.storageService.update(id, {
      status: newStatus
    });
  }

  onMenuClick($event: MouseEvent) {
    $event.stopPropagation();
  }

  onDelete(id: number) {
    if (this.deleteSubscr) {
      this.deleteSubscr.unsubscribe();
      this.deleteSubscr = null;
    }
    this.deleteSubscr = this.modalService.onOk$.subscribe(() => {
      this.storageService.remove(id);
      this.deleteSubscr.unsubscribe();
      this.modalService.open$.next(false);
    });
    this.modalService.open$.next(true);
  }
}
