export enum TaskStatus {
    NEW = "New",
    IN_PROGRESS = "In Progress",
    DONE = "Done"
}