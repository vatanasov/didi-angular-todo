import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListViewComponent } from './list-view/list-view.component';
import { EditFormComponent } from './edit-form/edit-form.component';
import { NotFoundComponent } from './not-found/not-found.component';


/**
 * /list - list + add
 * /edit/:id -> edit form
 * /404 for wrong pages
 */
const routes: Routes = [{
  path: 'list',
  component: ListViewComponent
}, {
  path: 'edit/:id',
  component: EditFormComponent
}, {
  path:'',
  pathMatch: 'full',
  redirectTo: '/list'
}, {
  path: '**',
  component: NotFoundComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
