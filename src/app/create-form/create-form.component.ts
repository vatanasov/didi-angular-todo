import { Component, OnInit } from '@angular/core';
import { StorageService } from '../services/storage.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-form',
  templateUrl: './create-form.component.html',
  styleUrls: ['./create-form.component.scss']
})
export class CreateFormComponent implements OnInit {

  public fg: FormGroup = new FormGroup({
    text: new FormControl('', [Validators.required, Validators.minLength(4)])
  });

  constructor(private storageService: StorageService) { }

  ngOnInit(): void {
  }

  onSave() {
    if (!this.fg.invalid) {
      const task = {
        text: this.fg.get('text').value
      };
      this.storageService.add(task);

      console.log(this.storageService.all());
    }
  }
}
