export interface StorageInterface<T> {
    /**
     * CRUD
     */

    add(item: T): T;

    get(id: number): T| null;

    update(id: number, properties: {[key: string]: any}): T | null;

    remove(id: number): boolean;

    all(): T[]

}