import { TestBed } from '@angular/core/testing';

import { StorageService } from './storage.service';
import { TaskStatus } from '../data/task-status.enum';

describe('StorageService', () => {
  let service: StorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
     // providers: [StorageService]
    });
    service = TestBed.inject(StorageService);
    StorageService.KEY = 0;
  });

  it('should be created', () => {
    expect(service !== undefined).toBeTruthy();
  });
// StorageService.add -> StorageService#add
  describe('#add', () => {
    it ('stores a task', () => {
      const task = service.add({
        text: 'Test'
      });
      // 1) ima li id
      expect(task.id).toBeTruthy();
      // 2) ima li status NEW
      expect(task.status).toEqual(TaskStatus.NEW)
      // 3) texta dali otgovarq
      expect(task.text).toEqual('Test');
      // 4) w kolekciqta li e ? 
      expect(service.get(task.id)).not.toBeNull(); 
    });
  });

  describe('#get', () => {
    beforeEach(() => {
      service.add({
        text: 'Test'
      });
    });

    it('returns object for given key', () => {
      const task = service.get(StorageService.KEY);
      expect(task).not.toBeNull();
      expect(task.id).toEqual(StorageService.KEY);
    });

    it ('retuns null for missing keys', () => {
      const task = service.get(StorageService.KEY + 1);
      expect(task).toBeNull();
    });
  });

  describe('#update', () => {
    beforeEach(() => {
      service.add({
        text: 'Test'
      });
    })
    it ('Updates given task', () => {
        service.update(1, {
          id: 15,
          text: 'Update',
          status: TaskStatus.DONE,
          falsePropery: undefined
        });

        const task = service.get(1);
        expect(task.id).toEqual(1);
        expect(task.text).toEqual('Update');
        expect(task.status).toEqual(TaskStatus.DONE);
        expect('falsePropery' in task).toEqual(false);
    });
  });
});
