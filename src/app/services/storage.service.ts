import { Injectable } from '@angular/core';
import { StorageInterface } from './storage.interface';
import { Task } from '../data/task.model';
import { TaskStatus } from '../data/task-status.enum';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService implements StorageInterface<Task>{

  /**
   * Observable emiting when data is changed 
   * add/update/remove so user can subscribe to hidrate data on demand
   */
  public readonly onDataChanged$: Subject<any> = new Subject();

  /**
   * observable which emits all data when it is changed
   * add/update/remove
   */
  public readonly all$: BehaviorSubject<Task[]> = new BehaviorSubject([]);

  static KEY = 0;

  private _data: Map<number, Task> = new Map<number, Task>();

  constructor() { }

  add(task: Task): Task {
    const id = ++StorageService.KEY;
    task.id = id;
    task.status = TaskStatus.NEW;
    this._data.set(id, task);

    this.onDataChanged$.next(null);
    this.all$.next(this.all());
    return task;
  }

  get(id: number): Task | null {
    if (this._data.has(id)) {
      return this._data.get(id);
    }

    return null;
  }

  update(id: number, properties: Task): Task | null {
    if (!this._data.has(id)) {
      return  null;
    }

    const task: Task = this._data.get(id);
    for (const key in properties) {
      if (task.hasOwnProperty(key) && key !== 'id') {
        task[key] = properties[key];
      }
    }

    this._data.set(id, task);
    this.onDataChanged$.next(null);
    this.all$.next(this.all());
    return task;
  }

  remove(id: number): boolean {
    if (!this._data.has(id)) {
      return false;
    }

    this._data.delete(id);

    this.onDataChanged$.next(null);
    this.all$.next(this.all());
    return true;
  }

  all(): Task[] {
   // map.entries [[1, Task], [2, Task]]
   return Array.from(this._data)
            .map((item: [number, Task]) => item[1]);
  }

  lastId(): number {
    return StorageService.KEY;
  }
}
