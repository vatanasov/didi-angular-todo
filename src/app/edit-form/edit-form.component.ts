import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TaskStatus } from '../data/task-status.enum';
import { StorageService } from '../services/storage.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.scss']
})
export class EditFormComponent implements OnInit {

  public fg: FormGroup = new FormGroup({
    text: new FormControl('', [Validators.required, Validators.minLength(4)]),
    status: new FormControl(TaskStatus.NEW)
  })

  public TaskStatus = TaskStatus;

  constructor(private storageService: StorageService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    const task = this.storageService.get(parseInt(id));
    if (!task) {
      this.router.navigate(['/404']);
    }

    this.fg.patchValue({
      text: task.text,
      status: task.status
    });
  }

  onSave() {
    const id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.storageService.update(id, {
      text: this.fg.get('text').value,
      status: this.fg.get('status').value
    });

    this.router.navigate(['/']);
  }

}
