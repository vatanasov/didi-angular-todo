import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ConfirmModalService {
    open$: BehaviorSubject<any> = new BehaviorSubject(false);
    onOk$: Subject<any> = new Subject();
    onCancel$: Subject<any> = new Subject();
}