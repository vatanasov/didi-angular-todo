import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConfirmModalService } from './confirm-modal.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent implements OnInit, OnDestroy {

  isOpen = false;

  subscription: Subscription;

  constructor(public modalService: ConfirmModalService) { }

  ngOnInit(): void {
    this.subscription = this.modalService.open$
      .subscribe(v => {
        console.log(v);
        this.isOpen = v
      });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onOk() {
    this.modalService.onOk$.next();
  }

  onCancel() {
    this.modalService.onCancel$.next();
  }
}
