import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lineRange'
})
export class LineRangePipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    let start: number = parseInt(args[0] as string);
    let limit: number = parseInt(args[1] as string);
    start = start || 0;
    limit = isNaN(limit) ? value.length : limit;

    return value.split("\n")
      .slice(start, limit)
      .join("\n");
  }

}
